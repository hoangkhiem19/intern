import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import store from './src/appRedux/store';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaView } from 'react-native';
import AppView from './src/modules/AppView';

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <SafeAreaView style={{ flex: 1 }}>
          <AppView />
        </SafeAreaView>
      </NavigationContainer>
    </Provider>
  );
}
