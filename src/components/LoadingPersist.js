import React from 'react';
import { Layout, Text } from '@ui-kitten/components';

export default function LoadingPersist() {
  return (
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>LoadingPersist</Text>
    </Layout>
  );
}
