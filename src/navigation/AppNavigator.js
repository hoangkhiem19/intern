import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  BottomNavigation,
  BottomNavigationTab,
  Icon,
} from '@ui-kitten/components';

import TrafsecCamera from '../modules/trafsec/camera/CameraView';
import TrafsecDetect from '../modules/trafsec/detect/DetectView';
import Notification from '../modules/notification/NotificationView';
import Setting from '../modules/setting/SettingView';
import MyVMSNavigator from './MyVMSNavigator';

const {Navigator, Screen} = createBottomTabNavigator();

const VideoIcon = (props) => <Icon {...props} name="video-outline" />;
const BellIcon = (props) => <Icon {...props} name="bell-outline" />;
const LayerIcon = (props) => <Icon {...props} name="layers-outline" />;
const MenuIcon = (props) => <Icon {...props} name="menu-outline" />;

const BottomTabBar = ({navigation, state}) => (
  <BottomNavigation
    selectedIndex={state.index}
    onSelect={(index) => navigation.navigate(state.routeNames[index])}>
    <BottomNavigationTab icon={VideoIcon} />
    <BottomNavigationTab icon={LayerIcon} />
    <BottomNavigationTab icon={BellIcon} />
    <BottomNavigationTab icon={MenuIcon} />
  </BottomNavigation>
);

export const AppNavigator = () => (
  <Navigator tabBar={(props) => <BottomTabBar {...props} />}>
    <Screen name="TrafsecCamera" component={MyVMSNavigator} />
    <Screen name="TrafsecDetect" component={TrafsecDetect} />
    <Screen name="Notification" component={Notification} />
    <Screen name="Setting" component={Setting} />
  </Navigator>
);
