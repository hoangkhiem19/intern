import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import ListCameraScreen from '../modules/trafsec/camera/CameraView';
import CameraDetailScreen from '../modules/trafsec/camera/CameraDetailView';

const Stack = createStackNavigator();

export default function MyVMSNavigator() {
  return (
    <Stack.Navigator
      // initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        cardOverlayEnabled: true,
        ...TransitionPresets.ModalPresentationIOS,
      }}
      mode="modal"
    >
      <Stack.Screen
        name="ListCamera"
        // options={{ title: 'Danh sách camera' }}
        component={ListCameraScreen}
      />
      <Stack.Screen
        name="CameraDetail"
        // options={{ title: 'Chi tiết camera' }}
        component={CameraDetailScreen}
      />
    </Stack.Navigator>
  );
}
