import axios from 'axios';
import store from '../appRedux/store';
import { DOMAIN } from './server';

const encode = (o) => {
  var q = [];
  for (var k in o)
    o.hasOwnProperty(k) &&
      q.push(encodeURIComponent(k) + '=' + encodeURIComponent(o[k]));
  return q.join('&');
};

const api = axios.create({
  baseURL: `https://${DOMAIN}/proxy`,
});

const handleError = (err) => {
  console.log(`handleError:${JSON.stringify(err)}`);
};

api.interceptors.request.use(function (config) {
  let session = null;

  try {
    session = store.getState().auth.currentUser.session.key;
  } catch (err) {
    console.log(`attachSession:${JSON.stringify(err)}`);
  }
  if (session) {
    config.headers.session = session;
  }

  return config;
});

api.interceptors.response.use(
  (res) => res.data,
  function (err) {
    handleError(err);
    return Promise.reject(err);
  },
);

export default api;

export { encode };
