import api, {encode} from '../api';
import moment from 'moment';

export const aoService = {
  get,
  getId,
  stats,
};

function stats(type, timeFrom, timeTo, object, camera) {
  var q = {'page[size]': 0};
  var f = [];

  type && (q['fields[ao_stats]'] = type);

  timeFrom && f.push({name: 'created_at', op: 'ge', val: timeFrom + '+07:00'});

  timeTo && f.push({name: 'created_at', op: 'le', val: timeTo + '+07:00'});

  object && f.push({name: 'object_type', op: 'in', val: object});

  camera && f.push({name: 'stream_id', op: 'in', val: camera});

  f.length > 0 && (q.filter = JSON.stringify(f));

  return api.get('api/trafsec/ao_history/stats?' + encode(q));
}

function getId(id) {
  return api.get(`api/trafsec/alerts/${id}?include=detected_object`);
}

function get(body) {
  const {
    pageNumber,
    pageSize,
    timeFrom,
    timeTo,
    object,
    camera,
    rule,
    plate,
    ignoreWrongLP,
    probLPFrom,
    probLPTo,
    probObjFrom,
    probObjTo,
  } = body;

  var q = {sort: '-created_at', include: 'detected_object'};
  var f = [];

  pageNumber ? (q['page[number]'] = pageNumber) : (q['page[number]'] = 1);

  pageSize ? (q['page[size]'] = pageSize) : (q['page[size]'] = 16);

  timeFrom
    ? f.push({name: 'created_at', op: 'ge', val: timeFrom})
    : f.push({
        name: 'created_at',
        op: 'ge',
        val: moment().format('YYYY-MM-DDT00:00:00ICT'),
      });
  timeTo && f.push({name: 'created_at', op: 'le', val: timeTo});

  object && f.push({name: 'name', op: 'in', val: object});

  camera && f.push({name: 'stream_id', op: 'in', val: camera});

  rule &&
    f.push({
      name: 'rule',
      op: 'has',
      val: {name: 'tags', op: 'contains', val: '{"' + rule + '"}'},
    });

  plate &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {
        name: '_attributes',
        op: 'any',
        val: {
          name: '_value',
          op: 'ilike',
          val: plate.includes('%') ? plate : '%' + plate + '%',
        },
      },
    });

  ignoreWrongLP &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {
        name: '_attributes',
        op: 'any',
        val: {
          name: '_value',
          op: 'notin_',
          val: ['DETECT-FAIL', 'RECOGNIZE-FAIL'],
        },
      },
    });

  probLPFrom &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {
        name: '_attributes',
        op: 'any',
        val: {name: 'prob', op: 'ge', val: probLPFrom / 100},
      },
    });

  probLPTo &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {
        name: '_attributes',
        op: 'any',
        val: {name: 'prob', op: 'le', val: probLPTo / 100},
      },
    });

  probObjFrom &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {name: 'prob', op: 'ge', val: probObjFrom / 100},
    });

  probObjTo &&
    f.push({
      name: 'detected_object',
      op: 'has',
      val: {name: 'prob', op: 'le', val: probObjTo / 100},
    });

  f.length > 0 && (q.filter = JSON.stringify(f));

  return api.get(`api/trafsec/alerts?` + encode(q));
}
