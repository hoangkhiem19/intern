import api from '../api';

export const meVmsService = {
  find,
  getTag,
  getLocation,
};

function find(params) {
  return api.get(`me/vms`, params);
}

function getTag(params) {
  return api.get(`me/vms/tags`, params);
}

function getLocation(params) {
  return api.get(`me/vms/locations`, params);
}
