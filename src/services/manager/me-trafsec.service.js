import api from '../api';

export const meTrafsecService = {
  find,
  getTag,
  getLocation,
};

function find(params) {
  return api.get(`me/trafsec`, params);
}

function getTag(params) {
  return api.get(`me/trafsec/tags`, params);
}

function getLocation(params) {
  return api.get(`me/trafsec/locations`, params);
}
