import React from 'react';
import { Layout, Text } from '@ui-kitten/components';

export default function Violation(props) {
  return (
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>TrafsecViolation</Text>
    </Layout>
  );
}
