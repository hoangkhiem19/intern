import React, { useEffect, useState } from 'react';
import { Layout, Card, Text } from '@ui-kitten/components';
import { View, Image, StyleSheet, Dimensions } from 'react-native';
export default function Camera(props) {
  let { vmsCamera } = props;
  let [WSData, setWSData] = useState({
    cameraThumbnail: vmsCamera.cameraThumbnail,
    licensePlate: 'Đang chờ tín hiệu',
    catchedAt: 'Đang chờ tín hiệu',
  });

  const navigateToDetail = (vmsCamera) => {
    props.navigation.navigate('CameraDetail', { vmsCamera });
  };

  function fetchWS(vmsCamera) {
    let ws = new WebSocket(vmsCamera.WSUrl);
    ws.onopen = () => {};
    ws.onmessage = (e) => {
      let detected_objects = JSON.parse(e.data);
      let cameraThumbnail = `https://mekongsmartcam.vnpttiengiang.vn/proxy/api/trafsec/images/${
        detected_objects.frame_id
      }?crop=0&thumbnail=1&bbox=${detected_objects.bbox.join(',')}`;
      let licensePlate = detected_objects.attr[0].value;
      if (licensePlate !== 'DETECT-FAIL' && licensePlate !== 'RECOGNIZE-FAIL') {
        setWSData({
          cameraThumbnail: cameraThumbnail,
          licensePlate: licensePlate,
          catchedAt: 'Vài giây trước',
        });
      }
    };
    ws.onerror = (e) => {};
    ws.onclose = (e) => {};
  }
  // fetchWS(vmsCamera);
  useEffect(() => {
    fetchWS(vmsCamera);
  }, []);

  const header = () => (
    <View style={styles.header}>
      <Text
        style={[styles.cameraInformation, styles.licensePlate]}
        category="h5"
      >
        {WSData.licensePlate}
      </Text>
      <Text style={[styles.cameraInformation, styles.catchedAt]} category="h5">
        {WSData.catchedAt}
      </Text>
    </View>
  );

  const footer = () => (
    <View style={styles.footer}>
      <Text style={styles.cameraName} category="h5">
        {vmsCamera.name}
      </Text>
    </View>
  );

  return (
    <Card
      style={styles.container}
      onPress={() => navigateToDetail(vmsCamera)}
      header={header}
      footer={footer}
      status="primary"
    >
      <Image
        source={{
          uri: WSData.cameraThumbnail,
        }}
        style={styles.thumbnail}
      />
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Dimensions.get('window').width - 10,
    position: 'relative',
    marginVertical: 5,
  },
  header: {
    position: 'relative',
    height: 50,
  },
  licensePlate: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  catchedAt: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  cameraName: {
    textTransform: 'uppercase',
    textAlign: 'center',
    padding: 5,
    fontWeight: '700',
  },
  thumbnail: { width: '100%', height: 200 },
  footer: {},
});
