import React, {useEffect} from 'react';
import {Layout, Text, List, Divider, Spinner} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchRequest as fetchMyVMSRequest} from '../../../appRedux/actions/MyVMS';
import {fetchRequest as fetchMyTrafsecRequest} from '../../../appRedux/actions/MyTrafsec';
import CameraWS from './CameraWS';
export default function Camera(props) {
  const dispatch = useDispatch();
  const vmsData = useSelector(({myVMS}) => myVMS);
  const trafsecData = useSelector(({myTrafsec}) => myTrafsec);

  useEffect(() => {
    dispatch(fetchMyVMSRequest({$limit: 65536}));
    dispatch(fetchMyTrafsecRequest({$limit: 65536}));
  }, []);

  const renderItem = ({item}) => {
    let match = trafsecData.data.filter(
      (trafSecItem) => trafSecItem.cameraId === item.cameraId,
    );
    item.WSUrl = `wss://mekongsmartcam.vnpttiengiang.vn/proxy/ws/trafsec/detected_objects/${match[0].refId}`;
    item.streamUrl = `https://mekongsmartcam.vnpttiengiang.vn/proxy/stream/${item.application}/${item.streamName}/playlist.m3u8`;
    item.detectedObjectsWSUrl = `wss://mekongsmartcam.vnpttiengiang.vn/proxy/ws/trafsec/detected_objects/${match[0].refId}`;

    return <CameraWS vmsCamera={item} navigation={props.navigation} />;
  };

  console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

  return (
    <Layout style={styles.container}>
      {vmsData.isLoading && <Spinner size="large" />}
      {vmsData.data.length > 0 && trafsecData.data.length > 0 && (
        <List data={vmsData.data} renderItem={renderItem} />
      )}
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  camera: {
    width: '100%',
    position: 'relative',
    marginVertical: 5,
  },
  cameraName: {
    textTransform: 'uppercase',
    textAlign: 'center',
    padding: 5,
    fontWeight: '700',
  },
  thumbnail: {width: '100%', height: 300},
  footer: {
    position: 'relative',
    height: 50,
  },
  licensePlate: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  catchedAt: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
});
