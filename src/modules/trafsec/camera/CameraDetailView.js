import React, { useEffect, useState } from 'react';
import { Layout, Card, Text, List, Divider } from '@ui-kitten/components';
import { View, Image, StyleSheet } from 'react-native';
import Video from 'react-native-video';
export default function CameraDetail(props) {
  let [data, setData] = useState([]);

  useEffect(() => {
    let ws = new WebSocket(vmsCamera.detectedObjectsWSUrl);
    const maxRecord = 10;
    ws.onopen = () => {};
    ws.onmessage = (e) => {
      let dataObj = JSON.parse(e.data);
      let bboxStr = dataObj.bbox;
      dataObj.noDropThumbnailUrl = `https://mekongsmartcam.vnpttiengiang.vn/proxy/api/trafsec/images/${
        dataObj.frame_id
      }?crop=0&thumbnail=1&bbox=${bboxStr.join(',')}`;
      dataObj.droppedThumbnailUrl = `https://mekongsmartcam.vnpttiengiang.vn/proxy/api/trafsec/images/${
        dataObj.frame_id
      }?crop=1&thumbnail=1&bbox=${bboxStr.join(',')}`;
      if (data.length > maxRecord) {
        data.shift();
      }
      data.push(dataObj);
      setData([...data]);
    };
    ws.onerror = (e) => {};
    ws.onclose = (e) => {};
  }, []);

  let { vmsCamera } = props.route.params;
  return (
    <Layout style={styles.container}>
      <Card
        style={styles.camera}
        header={() => (
          <View style={styles.header}>
            <Text style={styles.cameraName} category="h5">
              {vmsCamera.name}
            </Text>
          </View>
        )}
        status="primary"
      >
        <Video
          source={{
            uri: vmsCamera.streamUrl,
          }}
          style={{ height: 200 }}
          // controls={true}
          resizeMode={'cover'}
        />
      </Card>
      <Card style={styles.camera} status="danger">
        <List
          style={styles.detectedObjectList}
          data={data}
          renderItem={({ item }) => (
            <Layout style={styles.object}>
              <Image
                style={styles.noDropThumbnail}
                source={{ uri: item.noDropThumbnailUrl }}
              />
              <Image
                style={styles.droppedThumbnail}
                source={{ uri: item.droppedThumbnailUrl }}
              />
              <Text style={styles.licensePlate}>{item.attr[0].value}</Text>
            </Layout>
          )}
          keyExtractor={(item) => {
            return item.attr[0].id;
          }}
        />
      </Card>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 5,
  },
  camera: {
    width: '100%',
    position: 'relative',
    marginVertical: 5,
  },
  cameraName: {
    textTransform: 'uppercase',
    textAlign: 'center',
    padding: 5,
    fontWeight: '700',
  },
  thumbnail: { width: '100%', height: 300 },
  detectedObjectList: {},
  object: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    margin: 5,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
  },
  noDropThumbnail: {
    width: 100,
    height: 70,
    margin: 5,
  },
  droppedThumbnail: {
    width: 100,
    height: 70,
    margin: 5,
  },
  licensePlate: {
    margin: 5,
    justifyContent: 'center',
  },
});
