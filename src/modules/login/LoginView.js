import React, { useState } from 'react';
import { Button, Input, Layout, Text } from '@ui-kitten/components';

import { useDispatch, useSelector } from 'react-redux';
import { loginRequest } from '../../appRedux/actions/Auth';

export default function Login(props) {
  const dispatch = useDispatch();
  const { isLoggingIn } = useSelector(({ auth }) => auth);

  const [email, setEmail] = useState('thuctap@gmail.com');
  const [password, setPassword] = useState('Vnpt@123');

  return (
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Input
        onChangeText={setEmail}
        placeholder="email"
        value="thuctap@gmail.com"
      />
      <Input
        onChangeText={setPassword}
        placeholder="password"
        value="Vnpt@123"
      />
      <Button onPress={() => dispatch(loginRequest({ email, password }))}>
        login
      </Button>
      {isLoggingIn && <Text>isLoggingIn</Text>}
    </Layout>
  );
}
