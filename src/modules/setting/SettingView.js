import React from 'react';
import { Layout, Toggle } from '@ui-kitten/components';
import { useDispatch, useSelector } from 'react-redux';
import { toggleDarkMode } from '../../appRedux/actions/Common';

export default function Login(props) {
  const dispatch = useDispatch();
  const { darkMode } = useSelector(({ common }) => common);

  return (
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Toggle onChange={() => dispatch(toggleDarkMode())} checked={darkMode}>
        darkMode
      </Toggle>
    </Layout>
  );
}
