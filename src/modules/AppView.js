import React, { useEffect } from 'react';
import { LoadingPersist } from '../components';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from '../appRedux/store';

import { Platform, UIManager } from 'react-native';

import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';

import VerifyView from './verify/VerifyView';
import LoginView from './login/LoginView';
import { AppNavigator } from '../navigation/AppNavigator';
import { default as blueTheme } from '../theme/blue-theme.json';
import { default as backgroundBase } from '../theme/background-base.json';

import { useSelector } from 'react-redux';

export default function AppView(props) {
  useEffect(() => {
    if (Platform.OS === 'android') {
      // eslint-disable-next-line no-unused-expressions
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }, []);

  const { currentUser, isVerified } = useSelector(({ auth }) => auth);
  const { darkMode } = useSelector(({ common }) => common);

  let View = <AppNavigator />;
  if (currentUser == null) {
    View = <LoginView />;
  } else if (!isVerified) {
    View = <VerifyView />;
  }

  const defaultTheme = darkMode ? eva.dark : eva.light;
  const combineTheme = { ...defaultTheme, ...blueTheme, ...backgroundBase };

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={combineTheme}>
        <PersistGate
          loading={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <LoadingPersist />
          }
          persistor={persistor}>
          {View}
        </PersistGate>
      </ApplicationProvider>
    </>
  );
}
