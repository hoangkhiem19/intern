import React, { useEffect } from 'react';
import { Layout, Text } from '@ui-kitten/components';
import { useDispatch } from 'react-redux';
import { verifyRequest } from '../../appRedux/actions/Auth';

export default function Verify(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(verifyRequest());
  }, []);

  return (
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Verify</Text>
    </Layout>
  );
}
