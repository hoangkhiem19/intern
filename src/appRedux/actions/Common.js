import { TOGGLE_DARK_MODE } from '../constants/Common';

export const toggleDarkMode = () => {
  return {
    type: TOGGLE_DARK_MODE,
  };
};
