import { FETCH_REQUEST, FETCH_SUCCESS } from '../constants/MyVMS';

export const fetchRequest = (body) => {
  return {
    type: FETCH_REQUEST,
    ...body,
  };
};
