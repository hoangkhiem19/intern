import { FETCH_REQUEST, FETCH_SUCCESS } from '../constants/MyTrafsec';

export const fetchRequest = (body) => {
  return {
    type: FETCH_REQUEST,
    ...body,
  };
};
