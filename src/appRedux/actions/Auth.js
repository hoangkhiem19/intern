import {LOGIN_REQUEST, VERIFY_REQUEST} from '../constants/Auth';

export const loginRequest = ({email, password}) => {
  return {
    type: LOGIN_REQUEST,
    email,
    password,
  };
};

export const verifyRequest = () => {
  return {
    type: VERIFY_REQUEST,
  };
};
