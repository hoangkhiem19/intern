import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, compose, createStore} from 'redux';
import {persistStore} from 'redux-persist';
import {createLogger} from 'redux-logger';
import rootSaga from '../sagas/index';
import rootReducer from '../reducers';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger({
  collapsed: true,
  predicate: () => __DEV__,
});
const middlewares = [sagaMiddleware, loggerMiddleware];

const composeSetup =
  (__DEV__ &&
    typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const store = createStore(
  rootReducer,
  composeSetup(applyMiddleware(...middlewares)),
);
sagaMiddleware.run(rootSaga);

export default store;

export const persistor = persistStore(store);
