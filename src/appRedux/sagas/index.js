import {all} from 'redux-saga/effects';
import authSagas from './Auth';
import myVMSSagas from './MyVMS';
import myTrafsecSagas from './MyTrafsec';

export default function* rootSaga(getState) {
  yield all([authSagas(), myVMSSagas(), myTrafsecSagas()]);
}
