import { useSelector } from 'react-redux';
import { all, fork, put, takeLatest } from 'redux-saga/effects';
import { FETCH_REQUEST, FETCH_SUCCESS } from '../constants/MyTrafsec';
import { meTrafsecService } from '../../services/manager/me-trafsec.service';

function* fetchFlow(action) {
  try {
    const { $limit } = action;
    const data = yield meTrafsecService.find({ $limit });
    // console.log(`fetchTrafsecFlowDATA:${JSON.stringify(data.data)}`);
    yield put({ type: FETCH_SUCCESS, data: data.data });
  } catch (error) {
    console.log(`fetchTrafsecFlowERR:${JSON.stringify(error.message)}`);
  }
}

export function* fetchWatcher() {
  yield takeLatest(FETCH_REQUEST, fetchFlow);
}

export default function* rootSaga() {
  yield all([fork(fetchWatcher)]);
}
