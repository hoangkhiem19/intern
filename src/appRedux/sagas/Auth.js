import { all, fork, put, takeLatest } from 'redux-saga/effects';

import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  VERIFY_SUCCESS,
  VERIFY_ERROR,
  VERIFY_REQUEST,
} from '../constants/Auth';

import api from '../../services/api';

function* loginFlow(action) {
  try {
    const { email, password } = action;

    const data = yield api.post('/login', {
      email,
      password,
    });
    console.log(`loginFlow:${JSON.stringify(data)}`);

    yield put({ type: LOGIN_SUCCESS, data });
  } catch (error) {
    console.log(`loginFlow:${JSON.stringify(error)}`);
    yield put({ type: LOGIN_ERROR });
  }
}

export function* loginWatcher() {
  yield takeLatest(LOGIN_REQUEST, loginFlow);
}

function* verifyFlow(action) {
  try {
    const data = yield api.get('/me');
    console.log(`verifyFlow:${JSON.stringify(data)}`);

    yield put({ type: VERIFY_SUCCESS, data });
  } catch (error) {
    console.log(`verifyFlow:${JSON.stringify(error)}`);
    yield put({ type: VERIFY_ERROR });
  }
}

export function* verifyWatcher() {
  yield takeLatest(VERIFY_REQUEST, verifyFlow);
}

export default function* rootSaga() {
  yield all([fork(loginWatcher), fork(verifyWatcher)]);
}
