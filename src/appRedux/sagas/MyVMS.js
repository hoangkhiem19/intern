import { all, fork, put, takeLatest } from 'redux-saga/effects';
import { FETCH_REQUEST, FETCH_SUCCESS } from '../constants/MyVMS';
import { meVmsService } from '../../services/manager/me-vms.service';

function* fetchFlow(action) {
  try {
    const { $limit } = action;
    const data = yield meVmsService.find({ $limit });
    // console.log(`fetchVMSFlowDATA:${JSON.stringify(data)}`);

    // camera thumbnail
    data.data.map((o, i) => {
      o.cameraThumbnail = `https://mekongsmartcam.vnpttiengiang.vn/proxy/thumbnail/application=${o.application}&streamname=${o.streamName}&size=426x240&fitmode=stretch`;
    });
    yield put({ type: FETCH_SUCCESS, data: data.data });
  } catch (error) {
    console.log(`fetchVMSFlowErr:${JSON.stringify(error.message)}`);
  }
}

export function* fetchWatcher() {
  yield takeLatest(FETCH_REQUEST, fetchFlow);
}

export default function* rootSaga() {
  yield all([fork(fetchWatcher)]);
}
