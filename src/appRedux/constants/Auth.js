export const LOGIN_REQUEST = 'AuthAction/LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'AuthAction/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'AuthAction/LOGIN_ERROR';
export const VERIFY_REQUEST = 'AuthAction/VERIFY_REQUEST';
export const VERIFY_SUCCESS = 'AuthAction/VERIFY_SUCCESS';
export const VERIFY_ERROR = 'AuthAction/VERIFY_ERROR';
