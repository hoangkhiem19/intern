import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  VERIFY_SUCCESS,
  VERIFY_ERROR,
} from '../constants/Auth';

const initialState = {
  currentUser: null,
  isLoggingIn: false,
  isVerified: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoggingIn: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        currentUser: action.data,
        isLoggingIn: false,
        isVerified: true,
      };
    case LOGIN_ERROR:
      return state;
    case VERIFY_SUCCESS:
      return {
        ...state,
        currentUser: { ...state.currentUser, ...action.data },
        isVerified: true,
      };
    case VERIFY_ERROR:
      return state;
    default:
      return state;
  }
};
