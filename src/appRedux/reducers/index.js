import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import AsyncStorage from '@react-native-community/async-storage';

// ## Generator Reducer Imports
import Auth from './Auth';
import Common from './Common';
import MyVMS from './MyVMS';
import MyTrafsec from './MyTrafsec';

const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: ['currentUser'],
  stateReconciler: autoMergeLevel2,
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, Auth),
  common: Common,
  myVMS: MyVMS,
  myTrafsec: MyTrafsec,
});

const rootPersistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['common'],
  stateReconciler: autoMergeLevel2,
};

export default persistReducer(rootPersistConfig, rootReducer);
